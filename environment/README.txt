README >>
--------
Pei-Ju (Janifer) Lai
s3724265
------------------
				CONTROLS
WASD to move player

Left Click to interact with object

------------------

				PROJECT
My project is an alleyway with an brick illusion wall into a secret hidden
garden throne room.

				GOALS
Front
A typical alleyway which one can see should they walk around their 
neighbourhood. With odd elements which are out of place, encouraging people 
to follow the trail to the 'brick illusion wall'.

Brick Illusion Wall:
To be able to pull off a believable brick wall from the end of the alleyway,
of which should people choose to follow the trail, people realise it is 
actually just clever placement of existing walls.

Back:
A throne-esque looking room with nature, garden elements. A feeling that 
this is different, and later, when walking towards the somewhat obscuring 
fog, in the room with columns framing the border area and inside lamps 
and a grass carpet leading towards the back-center area. 

A feeling of foreboding, confusion, uncertainty.

------------------------------------------------------------------------------

All assets are my own, created in Maya 2018.