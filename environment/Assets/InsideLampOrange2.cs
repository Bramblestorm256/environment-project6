﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideLampOrange2 : MonoBehaviour
{
    public GameObject insideLampOrange2Object;

    public GameObject lampInsideOrangeText2;

    // Start is called before the first frame update
    void Start()
    {
        insideLampOrange2Object.GetComponent<Rigidbody>().useGravity = true;
        lampInsideOrangeText2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupInsideLampOrange2()
    {
        Debug.Log("HERE-Capsule");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            lampInsideOrangeText2.SetActive(true);

        }
    }// END OF PICKUP CAPSULE

    public void dropInsideLampOrange2()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            lampInsideOrangeText2.SetActive(false);
        }
    }
}
