﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Eight : MonoBehaviour
{
    public GameObject pCube2EightObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2EightObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2EightObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Eight()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Eight>().pCube2EightPoof();

        }
    }// END OF PICKUP CAPSULE

    public void dropCube2Eight()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
        }
    }

    public void pCube2EightPoof()
    {
        pCube2EightObject.SetActive(false);
    }
}
