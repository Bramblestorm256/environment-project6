﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capsule : MonoBehaviour
{
    public GameObject capsuleObject;

    //public GameObject capsuleText1;

    // Start is called before the first frame update
    void Start()
    {
        capsuleObject.GetComponent<Rigidbody>().useGravity = true;
        capsuleObject.SetActive(true);
        //capsuleText1.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pickupCapsule()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            //capsuleText1.SetActive(true);

            FindObjectOfType<Capsule>().capsulePoof();

        }
    }// END OF PICKUP CAPSULE


    public void dropCapsule()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            //capsuleText1.SetActive(false);
        }
    }

    public void capsulePoof()
    {
        capsuleObject.SetActive(false);
        //capsuleText1.SetActive(false);
    }

}
