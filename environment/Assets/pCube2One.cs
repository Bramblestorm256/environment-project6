﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2One : MonoBehaviour
{
    public GameObject pCube2OneObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2OneObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2OneObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2One()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2One>().pCube2OnePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2OnePoof()
    {
        pCube2OneObject.SetActive(false);
    }
}
