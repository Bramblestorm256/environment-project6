﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Five : MonoBehaviour
{
    public GameObject pCube2FiveObject;
    // Start is called before the first frame update
    void Start()
    {
        pCube2FiveObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2FiveObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Five()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Five>().pCube2FivePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2FivePoof()
    {
        pCube2FiveObject.SetActive(false);
    }
}
