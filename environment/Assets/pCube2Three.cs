﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Three : MonoBehaviour
{
    public GameObject pCube2ThreeObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2ThreeObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2ThreeObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Three()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Three>().pCube2ThreePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2ThreePoof()
    {
        pCube2ThreeObject.SetActive(false);
    }
}
