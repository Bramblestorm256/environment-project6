﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2FourNine : MonoBehaviour
{
    public GameObject pCube2FourNineObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2FourNineObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2FourNineObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pickupCube2FourNine()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2FourNine>().pCube2FourNinePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2FourNinePoof()
    {
        pCube2FourNineObject.SetActive(false);
    }
}
