﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideLampOrange3 : MonoBehaviour
{
    public GameObject insideLampOrange3Object;

    public GameObject lampInsideOrangeText4Clue;

    public GameObject streetLamp;
    public GameObject poster;

    public GameObject wallBlockFront;
    public GameObject wallBlockBack;

    // Start is called before the first frame update
    void Start()
    {
        insideLampOrange3Object.GetComponent<Rigidbody>().useGravity = true;
        lampInsideOrangeText4Clue.SetActive(false);
        //insideLampOrange3Object.SetActive(true);

        streetLamp.SetActive(false);
        poster.SetActive(false);

        wallBlockFront.SetActive(true);
        wallBlockBack.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupInsideLampOrange3()
    {
        Debug.Log("HERE-Capsule");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            lampInsideOrangeText4Clue.SetActive(true);
            //insideLampOrange3Object.SetActive(false);

            streetLamp.SetActive(true);
            poster.SetActive(true);

            wallBlockFront.SetActive(false);
            wallBlockBack.SetActive(false);

            //FindObjectOfType<InsideLampOrange3>().disappearLampO3();



        }
    }// END OF PICKUP CAPSULE

    /*
    public void disappearLampO3()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            insideLampOrange3Object.SetActive(false);
        }
    }
    */

    public void dropInsideLampOrange3()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            lampInsideOrangeText4Clue.SetActive(false);
        }
    }
}
