﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Two : MonoBehaviour
{
    public GameObject pCube2TwoObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2TwoObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2TwoObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Two()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Two>().pCube2TwoPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2TwoPoof()
    {
        pCube2TwoObject.SetActive(false);
    }
}
