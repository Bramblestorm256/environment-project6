﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class count : MonoBehaviour
{
    public int capsuleNumber;
    public GameObject[] capsuleBox;
    public GameObject capsuleNumb26;
    public GameObject capsuleNumb25;
    public GameObject capsuleNumb24;
    public GameObject capsuleNumb23;
    public GameObject capsuleNumb22;
    public GameObject capsuleNumb21;
    public GameObject capsuleNumb20;

    public GameObject capsuleNumb19;
    public GameObject capsuleNumb18;
    public GameObject capsuleNumb17;
    public GameObject capsuleNumb16;
    public GameObject capsuleNumb15;
    public GameObject capsuleNumb14;
    public GameObject capsuleNumb13;
    public GameObject capsuleNumb12;
    public GameObject capsuleNumb11;
    public GameObject capsuleNumb10;

    public GameObject capsuleNumb9;
    public GameObject capsuleNumb8;
    public GameObject capsuleNumb7;
    public GameObject capsuleNumb6;
    public GameObject capsuleNumb5;
    public GameObject capsuleNumb4;
    public GameObject capsuleNumb3;
    public GameObject capsuleNumb2;
    public GameObject capsuleNumb1;
    public GameObject capsuleNumb0;

    // Start is called before the first frame update
    void Start()
    {
        capsuleNumber = 26;

        capsuleNumb26.SetActive(true);
        capsuleNumb25.SetActive(false);
        capsuleNumb24.SetActive(false);
        capsuleNumb23.SetActive(false);
        capsuleNumb22.SetActive(false);
        capsuleNumb21.SetActive(false);
        capsuleNumb20.SetActive(false);

        capsuleNumb19.SetActive(false);
        capsuleNumb18.SetActive(false);
        capsuleNumb17.SetActive(false);
        capsuleNumb16.SetActive(false);
        capsuleNumb15.SetActive(false);
        capsuleNumb14.SetActive(false);
        capsuleNumb13.SetActive(false);
        capsuleNumb12.SetActive(false);
        capsuleNumb11.SetActive(false);
        capsuleNumb10.SetActive(false);

        capsuleNumb9.SetActive(false);
        capsuleNumb8.SetActive(false);
        capsuleNumb7.SetActive(false);
        capsuleNumb6.SetActive(false);
        capsuleNumb5.SetActive(false);
        capsuleNumb4.SetActive(false);
        capsuleNumb3.SetActive(false);
        capsuleNumb2.SetActive(false);
        capsuleNumb1.SetActive(false);
        capsuleNumb0.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void capsuleCount()
    {
        capsuleNumber -= 1;

        Debug.Log("CAPSULECOUNT");


        if (capsuleNumber == 25)
        {
            Debug.Log("25");
            capsuleNumb26.SetActive(false);
            capsuleNumb25.SetActive(true);
        }
        
        if (capsuleNumber == 24)
        {
            Debug.Log("24");
            capsuleNumb25.SetActive(false);
            capsuleNumb24.SetActive(true);
        }

        if (capsuleNumber == 23)
        {
            Debug.Log("23");
            capsuleNumb24.SetActive(false);
            capsuleNumb23.SetActive(true);
        }

        if (capsuleNumber == 22)
        {
            Debug.Log("22");
            capsuleNumb23.SetActive(false);
            capsuleNumb22.SetActive(true);
        }

        if (capsuleNumber == 21)
        {
            Debug.Log("21");
            capsuleNumb22.SetActive(false);
            capsuleNumb21.SetActive(true);
        }

        if (capsuleNumber == 20)
        {
            Debug.Log("20");
            capsuleNumb21.SetActive(false);
            capsuleNumb20.SetActive(true);
        }

        if (capsuleNumber == 19)
        {
            Debug.Log("19");
            capsuleNumb20.SetActive(false);
            capsuleNumb19.SetActive(true);
        }

        if (capsuleNumber == 18)
        {
            Debug.Log("18");
            capsuleNumb19.SetActive(false);
            capsuleNumb18.SetActive(true);
        }

        if (capsuleNumber == 17)
        {
            Debug.Log("17");
            capsuleNumb18.SetActive(false);
            capsuleNumb17.SetActive(true);
        }

        if (capsuleNumber == 16)
        {
            Debug.Log("16");
            capsuleNumb17.SetActive(false);
            capsuleNumb16.SetActive(true);
        }

        if (capsuleNumber == 15)
        {
            Debug.Log("15");
            capsuleNumb16.SetActive(false);
            capsuleNumb15.SetActive(true);
        }

        if (capsuleNumber == 14)
        {
            Debug.Log("14");
            capsuleNumb15.SetActive(false);
            capsuleNumb14.SetActive(true);
        }

        if (capsuleNumber == 13)
        {
            Debug.Log("13");
            capsuleNumb14.SetActive(false);
            capsuleNumb13.SetActive(true);
        }

        if (capsuleNumber == 12)
        {
            Debug.Log("12");
            capsuleNumb13.SetActive(false);
            capsuleNumb12.SetActive(true);
        }

        if (capsuleNumber == 11)
        {
            Debug.Log("11");
            capsuleNumb12.SetActive(false);
            capsuleNumb11.SetActive(true);
        }

        if (capsuleNumber == 10)
        {
            Debug.Log("10");
            capsuleNumb11.SetActive(false);
            capsuleNumb10.SetActive(true);
        }

        if (capsuleNumber == 9)
        {
            Debug.Log("9");
            capsuleNumb10.SetActive(false);
            capsuleNumb9.SetActive(true);
        }

        if (capsuleNumber == 8)
        {
            Debug.Log("8");
            capsuleNumb9.SetActive(false);
            capsuleNumb8.SetActive(true);
        }

        if (capsuleNumber == 7)
        {
            Debug.Log("7");
            capsuleNumb8.SetActive(false);
            capsuleNumb7.SetActive(true);
        }

        if (capsuleNumber == 6)
        {
            Debug.Log("6");
            capsuleNumb7.SetActive(false);
            capsuleNumb6.SetActive(true);
        }

        if (capsuleNumber == 5)
        {
            Debug.Log("5");
            capsuleNumb6.SetActive(false);
            capsuleNumb5.SetActive(true);
        }

        if (capsuleNumber == 4)
        {
            Debug.Log("4");
            capsuleNumb5.SetActive(false);
            capsuleNumb4.SetActive(true);
        }

        if (capsuleNumber == 3)
        {
            Debug.Log("3");
            capsuleNumb4.SetActive(false);
            capsuleNumb3.SetActive(true);
        }

        if (capsuleNumber == 2)
        {
            Debug.Log("2");
            capsuleNumb3.SetActive(false);
            capsuleNumb2.SetActive(true);
        }
         
        if (capsuleNumber == 1)
        {
            Debug.Log("1");
            capsuleNumb2.SetActive(false);
            capsuleNumb1.SetActive(true);
        }

        if (capsuleNumber == 0)
        {
            Debug.Log("0");
            capsuleNumb1.SetActive(false);
            capsuleNumb0.SetActive(true);
        }
         

    }
}
