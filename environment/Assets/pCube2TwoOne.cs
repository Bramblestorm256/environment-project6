﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2TwoOne : MonoBehaviour
{
    public GameObject pCube2TwoOneObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2TwoOneObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2TwoOneObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pickupCube2TwoOne()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2TwoOne>().pCube2TwoOnePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2TwoOnePoof()
    {
        pCube2TwoOneObject.SetActive(false);
    }
}
