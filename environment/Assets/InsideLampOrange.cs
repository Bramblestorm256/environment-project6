﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideLampOrange : MonoBehaviour
{
    public GameObject insideLampOrangeObject;

    public GameObject lampInsideText1;


    // Start is called before the first frame update
    void Start()
    {
        insideLampOrangeObject.GetComponent<Rigidbody>().useGravity = true;
        lampInsideText1.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupInsideLampOrange()
    {
        Debug.Log("HERE-Capsule");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            lampInsideText1.SetActive(true);

        }
    }// END OF PICKUP CAPSULE

    public void dropInsideLampOrange()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            lampInsideText1.SetActive(false);
        }
    }
}
