﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioJump : MonoBehaviour
{
    public AudioClip jumpSE;
    public AudioSource jumpSource;

    // Start is called before the first frame update
    void Start()
    {
        jumpSource.clip = jumpSE;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpSource.Play();
        }
    }
}
