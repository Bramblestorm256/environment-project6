﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2ThreeThree : MonoBehaviour
{
    public GameObject pCube2ThreeThreeObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2ThreeThreeObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2ThreeThreeObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pickupCube2ThreeThree()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2ThreeThree>().pCube2ThreeThreePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2ThreeThreePoof()
    {
        pCube2ThreeThreeObject.SetActive(false);
    }
}
