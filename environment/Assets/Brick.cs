﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public GameObject brickObject;

    public GameObject brickText1;

    // Start is called before the first frame update
    void Start()
    {
        brickObject.GetComponent<Rigidbody>().useGravity = true;
        brickText1.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupBrick()
    {
        Debug.Log("HERE-Capsule");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            brickText1.SetActive(true);

        }
    }// END OF PICKUP CAPSULE

    public void dropBrick()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            brickText1.SetActive(false);
        }
    }

}//End of class
