﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Nine : MonoBehaviour
{
    public GameObject pCube2NineObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2NineObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2NineObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Nine()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Nine>().pCube2NinePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2NinePoof()
    {
        pCube2NineObject.SetActive(false);
    }
}
