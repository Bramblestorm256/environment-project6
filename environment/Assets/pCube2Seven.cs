﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Seven : MonoBehaviour
{
    public GameObject pCube2SevenObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2SevenObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2SevenObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Seven()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Seven>().pCube2SevenPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2SevenPoof()
    {
        pCube2SevenObject.SetActive(false);
    }
}
