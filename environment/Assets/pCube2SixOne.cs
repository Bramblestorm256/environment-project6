﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2SixOne : MonoBehaviour
{

    public GameObject pCube2SixOneObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2SixOneObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2SixOneObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2SixOne()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2SixOne>().pCube2SixOnePoof();

        }
    }// END OF PICKUP CAPSULE

    public void dropCube2SixOne()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
        }
    }

    public void pCube2SixOnePoof()
    {
        pCube2SixOneObject.SetActive(false);
    }



}
