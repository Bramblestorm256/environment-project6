﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Ten : MonoBehaviour
{
    public GameObject pCube2TenObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2TenObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2TenObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Ten()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Ten>().pCube2TenPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2TenPoof()
    {
        pCube2TenObject.SetActive(false);
    }
}
