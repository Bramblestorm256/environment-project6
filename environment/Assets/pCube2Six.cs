﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Six : MonoBehaviour
{
    public GameObject pCube2SixObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2SixObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2SixObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Six()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Six>().pCube2SixPoof();

        }
    }// END OF PICKUP CAPSULE

    public void dropCube2Six()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
        }
    }

    public void pCube2SixPoof()
    {
        pCube2SixObject.SetActive(false);
    }



}
