﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Eleven : MonoBehaviour
{
    public GameObject pCube2ElevenObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2ElevenObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2ElevenObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void pickupCube2Eleven()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Eleven>().pCube2ElevenPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2ElevenPoof()
    {
        pCube2ElevenObject.SetActive(false);
    }

}
