﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Thirteen : MonoBehaviour
{
    public GameObject pCube2ThirteenObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2ThirteenObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2ThirteenObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Thirteen()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Thirteen>().pCube2ThirteenPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2ThirteenPoof()
    {
        pCube2ThirteenObject.SetActive(false);
    }

}
