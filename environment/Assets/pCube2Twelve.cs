﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Twelve : MonoBehaviour
{
    public GameObject pCube2TwelveObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2TwelveObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2TwelveObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Twelve()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Twelve>().pCube2TwelvePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2TwelvePoof()
    {
        pCube2TwelveObject.SetActive(false);
    }
}
