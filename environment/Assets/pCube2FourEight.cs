﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2FourEight : MonoBehaviour
{
    public GameObject pCube2FourEightObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2FourEightObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2FourEightObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2FourEight()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2FourEight>().pCube2FourEightPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2FourEightPoof()
    {
        pCube2FourEightObject.SetActive(false);
    }
}
