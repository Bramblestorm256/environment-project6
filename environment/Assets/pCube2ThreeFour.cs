﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2ThreeFour : MonoBehaviour
{
    public GameObject pCube2ThreeFourObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2ThreeFourObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2ThreeFourObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pickupCube2ThreeFour()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2ThreeFour>().pCube2ThreeFourPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2ThreeFourPoof()
    {
        pCube2ThreeFourObject.SetActive(false);
    }
}