﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Fourteen : MonoBehaviour
{
    public GameObject pCube2FourteenObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2FourteenObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2FourteenObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Fourteen()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Fourteen>().pCube2FourteenPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2FourteenPoof()
    {
        pCube2FourteenObject.SetActive(false);
    }

}
