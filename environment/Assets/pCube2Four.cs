﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2Four : MonoBehaviour
{
    public GameObject pCube2FourObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2FourObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2FourObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2Four()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2Four>().pCube2FourPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2FourPoof()
    {
        pCube2FourObject.SetActive(false);
    }
}
