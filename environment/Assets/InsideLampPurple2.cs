﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideLampPurple2 : MonoBehaviour
{
    public GameObject insideLampPurple2Object;

    public GameObject lampInsidePurpleText2;

    // Start is called before the first frame update
    void Start()
    {
        insideLampPurple2Object.GetComponent<Rigidbody>().useGravity = true;
        lampInsidePurpleText2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupInsideLampPurple2()
    {
        Debug.Log("HERE-Capsule");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            lampInsidePurpleText2.SetActive(true);

        }
    }// END OF PICKUP CAPSULE

    public void dropInsideLampPurple2()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            lampInsidePurpleText2.SetActive(false);
        }
    }
}
