﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2 : MonoBehaviour
{
    public GameObject pCube2Object;

    // Start is called before the first frame update
    void Start()
    {
        pCube2Object.GetComponent<Rigidbody>().useGravity = true;
        pCube2Object.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2>().pCube2Poof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2Poof()
    {
        pCube2Object.SetActive(false);
    }
}
