﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycasting : MonoBehaviour
{
    Transform thisTransform;

    public GameObject capsuleText1;

    public GameObject lampInsideOrangeText3;

    public GameObject hiddenWallText;

    public GameObject lampInsidePurpleText1;

    public GameObject lampInsidePurpleText2;

    public GameObject lampInsidePurpleText3;

    public GameObject lampInsidePurpleText4;


    // Start is called before the first frame update
    void Start()
    {
        thisTransform = this.transform;

        capsuleText1.SetActive(false);

        lampInsideOrangeText3.SetActive(false);

        hiddenWallText.SetActive(false);

        lampInsidePurpleText1.SetActive(false);

        lampInsidePurpleText2.SetActive(false);

        lampInsidePurpleText3.SetActive(false);

        lampInsidePurpleText4.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        capsuleText1.SetActive(false);

        lampInsideOrangeText3.SetActive(false);

        hiddenWallText.SetActive(false);

        lampInsidePurpleText1.SetActive(false);

        lampInsidePurpleText2.SetActive(false);

        lampInsidePurpleText3.SetActive(false);

        lampInsidePurpleText4.SetActive(false);

        RaycastHit hit;
        if (Physics.Raycast(thisTransform.position, thisTransform.forward, out hit))
        {
            Debug.Log(hit.transform.tag);

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "HiddenWall")
            {
                hiddenWallText.SetActive(true);
            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(35)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2ThreeFive>().pickupCube2ThreeFive();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(33)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2ThreeThree>().pickupCube2ThreeThree();
                }

            }
            
            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(34)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2ThreeFour>().pickupCube2ThreeFour();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(21)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2TwoOne>().pickupCube2TwoOne();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(49)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2FourNine>().pickupCube2FourNine();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(48)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2FourEight>().pickupCube2FourEight();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(47)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2FourSeven>().pickupCube2FourSeven();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(24)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2TwoFour>().pickupCube2TwoFour();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(43)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2FourtyThree>().pickupCube2FortyThree();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(14)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Fourteen>().pickupCube2Fourteen();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(13)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Thirteen>().pickupCube2Thirteen();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(12)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Twelve>().pickupCube2Twelve();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(2)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Two>().pickupCube2Two();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(3)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Three>().pickupCube2Three();
                }

            }
            
            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2One")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2One>().pickupCube2One();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2>().pickupCube2();
                }

            }
            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2DotOne")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2DotOne>().pickupCube2DotOne();
                }

            }
            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(4)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Four>().pickupCube2Four();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(5)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Five>().pickupCube2Five();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(7)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Seven>().pickupCube2Seven();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(10)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Ten>().pickupCube2Ten();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(9)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Nine>().pickupCube2Nine();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(11)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Eleven>().pickupCube2Eleven();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(8)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Eight>().pickupCube2Eight();
                }
            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(6)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2Six>().pickupCube2Six();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "pCube2(6.1)")
            {
                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<pCube2SixOne>().pickupCube2SixOne();
                }

            }

            //IF RAYCAST IS AT CAPSULE
            if (hit.transform.tag == "Capsule")
            {
                //Debug.Log("CAP");
                //Debug.Log("Capsule");

                capsuleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButton(0))
                {
                    //Debug.Log("HERE");
                    //FindObjectOfType<count>().capsuleCount();
                    FindObjectOfType<Capsule>().pickupCapsule();
                }
                
            }

            //IF RAYCAST HITS A BRICK
            if (hit.transform.tag == "Brick")
            {
                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
                    FindObjectOfType<Brick>().pickupBrick();
                }

                else
                {
                    FindObjectOfType<Brick>().dropBrick();
                }
            }

            //IF RAYCAST HITS INSIDE ORANGE LAMP
            if (hit.transform.tag == "InsideLamp")
            {
                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
                    FindObjectOfType<InsideLampOrange>().pickupInsideLampOrange();
                }
                 
                else
                {
                    FindObjectOfType<InsideLampOrange>().dropInsideLampOrange();
                }
            }

            //IF RAYCAST HITS INSIDE PURPLE LAMP
            if (hit.transform.tag == "InsideLampPurple")
            {
                lampInsidePurpleText1.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
                    FindObjectOfType<InsideLampPurple>().pickupInsideLampPurple();
                }

                else
                {
                    FindObjectOfType<InsideLampPurple>().dropInsideLampPurple();
                }
            }

            //IF RAYCAST HITS INSIDE PURPLE LAMP 2
            if (hit.transform.tag == "InsideLampPurple2")
            {
                lampInsidePurpleText2.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
                    FindObjectOfType<InsideLampPurple2>().pickupInsideLampPurple2();
                }

                else
                {
                    FindObjectOfType<InsideLampPurple2>().dropInsideLampPurple2();
                }
            }

            //IF RAYCAST HITS INSIDE PURPLE LAMP 3
            if (hit.transform.tag == "InsideLampPurple3")
            {
                lampInsidePurpleText3.SetActive(true);
                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
                    FindObjectOfType<InsideLampPurple3>().pickupInsideLampPurple3();
                }

                else
                {
                    FindObjectOfType<InsideLampPurple3>().dropInsideLampPurple3();
                }
            }

            //IF RAYCAST HITS INSIDE PURPLE LAMP 4
            if (hit.transform.tag == "InsideLampPurple4")
            {
                lampInsidePurpleText4.SetActive(true);
                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
                    FindObjectOfType<InsideLampPurple4>().pickupInsideLampPurple4();
                }

                else
                {
                    FindObjectOfType<InsideLampPurple4>().dropInsideLampPurple4();
                }
            }

            //IF RAYCAST HITS INSIDE ORANGE LAMP 2
            if (hit.transform.tag == "InsideLamp2")
            {
                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
                    FindObjectOfType<InsideLampOrange2>().pickupInsideLampOrange2();
                }

                else
                {
                    FindObjectOfType<InsideLampOrange2>().dropInsideLampOrange2();
                }
            }

            //IF RAYCAST HITS INSIDE ORANGE LAMP 3 --> Beginning one
            if (hit.transform.tag == "InsideLamp3")
            {
                lampInsideOrangeText3.SetActive(true);

                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
    
                    FindObjectOfType<InsideLampOrange3>().pickupInsideLampOrange3();
                }

                else
                {
                    FindObjectOfType<InsideLampOrange3>().dropInsideLampOrange3();
                }
            }
           
            //IF RAYCAST HITS STATUE
            if (hit.transform.tag == "Exit")
            {
                //IF MOUSE IS DOWN
                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("QUITTED");
                    Application.Quit();
                }
            }

            //TEST
            if (hit.transform.tag == "Test")
            {
                //Debug.Log("TESTTTTTTTT");

            }


        }


    }//End of Update

}
