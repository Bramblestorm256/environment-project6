﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideLampPurple : MonoBehaviour
{
    public GameObject insideLampPurpleObject;

    public GameObject lampInsidePurpleText1;

    // Start is called before the first frame update
    void Start()
    {
        insideLampPurpleObject.GetComponent<Rigidbody>().useGravity = true;
        lampInsidePurpleText1.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupInsideLampPurple()
    {
        Debug.Log("HERE-Capsule");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            lampInsidePurpleText1.SetActive(true);

        }
    }// END OF PICKUP CAPSULE

    public void dropInsideLampPurple()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            lampInsidePurpleText1.SetActive(false);
        }
    }
}
