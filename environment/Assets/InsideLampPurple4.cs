﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideLampPurple4 : MonoBehaviour
{
    public GameObject insideLampPurple4Object;

    public GameObject lampInsidePurpleText4;

    // Start is called before the first frame update
    void Start()
    {
        insideLampPurple4Object.GetComponent<Rigidbody>().useGravity = true;
        lampInsidePurpleText4.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupInsideLampPurple4()
    {
        Debug.Log("HERE-Capsule");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            lampInsidePurpleText4.SetActive(true);

        }
    }// END OF PICKUP CAPSULE

    public void dropInsideLampPurple4()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            lampInsidePurpleText4.SetActive(false);
        }
    }
}
