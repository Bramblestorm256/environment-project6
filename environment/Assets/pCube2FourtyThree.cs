﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2FourtyThree : MonoBehaviour
{
    public GameObject pCube2FourtyThreeObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2FourtyThreeObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2FourtyThreeObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2FortyThree()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2FourtyThree>().pCube2FourtyThreePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2FourtyThreePoof()
    {
        pCube2FourtyThreeObject.SetActive(false);
    }
}
