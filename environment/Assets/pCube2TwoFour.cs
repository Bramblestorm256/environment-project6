﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2TwoFour : MonoBehaviour
{
    public GameObject pCube2TwoFourObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2TwoFourObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2TwoFourObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2TwoFour()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2TwoFour>().pCube2TwoFourPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2TwoFourPoof()
    {
        pCube2TwoFourObject.SetActive(false);
    }
}
