﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2FourSeven : MonoBehaviour
{
    public GameObject pCube2FourSevenObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2FourSevenObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2FourSevenObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2FourSeven()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2FourSeven>().pCube2FourSevenPoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2FourSevenPoof()
    {
        pCube2FourSevenObject.SetActive(false);
    }
}
