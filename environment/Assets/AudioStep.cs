﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStep : MonoBehaviour
{
    public AudioClip footstepsSE;
    public AudioSource footstepsSource;

    // Start is called before the first frame update
    void Start()
    {
        footstepsSource.clip = footstepsSE;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            footstepsSource.Play();
        }
    }
}