﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2ThreeFive : MonoBehaviour
{
    public GameObject pCube2ThreeFiveObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2ThreeFiveObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2ThreeFiveObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pickupCube2ThreeFive()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2ThreeFive>().pCube2ThreeFivePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2ThreeFivePoof()
    {
        pCube2ThreeFiveObject.SetActive(false);
    }
}