﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideLampPurple3 : MonoBehaviour
{
    public GameObject insideLampPurple3Object;

    public GameObject lampInsidePurpleText3;

    // Start is called before the first frame update
    void Start()
    {
        insideLampPurple3Object.GetComponent<Rigidbody>().useGravity = true;
        lampInsidePurpleText3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupInsideLampPurple3()
    {
        Debug.Log("HERE-Capsule");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            lampInsidePurpleText3.SetActive(true);

        }
    }// END OF PICKUP CAPSULE

    public void dropInsideLampPurple3()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("UP");
            lampInsidePurpleText3.SetActive(false);
        }
    }
}
