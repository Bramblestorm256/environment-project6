﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pCube2DotOne : MonoBehaviour
{
    public GameObject pCube2DotOneObject;

    // Start is called before the first frame update
    void Start()
    {
        pCube2DotOneObject.GetComponent<Rigidbody>().useGravity = true;
        pCube2DotOneObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pickupCube2DotOne()
    {
        Debug.Log("HERE");
        //capsuleText1.SetActive(true);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("DOWN");

            FindObjectOfType<pCube2DotOne>().pCube2DotOnePoof();

        }
    }// END OF PICKUP CAPSULE

    public void pCube2DotOnePoof()
    {
        pCube2DotOneObject.SetActive(false);
    }
}
