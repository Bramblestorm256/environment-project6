Readme
Janifer (Pei-Ju) Lai
s3724265

----------------------------------------------------------------------------------------
Updated Git:
git clone https://Bramblestorm256@bitbucket.org/Bramblestorm256/environment-project6.git
git clone https://Bramblestorm256@bitbucket.org/Bramblestorm256/environment-project6.git

Pre-Git, Old Google Drive with Assets and previous environment updates - Includes first
rendition of environment in Project 6:
https://drive.google.com/drive/folders/1u8LogN3XqbxKSJ8ZNN_A1CwvC8cKX3Rk?usp=sharing 

----------------------------------------------------------------------------------------

Project 6 - Extended Project 5

----------------------------------------------------------------------------------------
Player Controls:
WASD --> To move around
Spacebar --> To jump
Mouse --> To look around
MB1 --> To click and interact with environment.

----------------------------------------------------------------------------------------
Creative Goal:
An environment that interacts with the player, instead of the other way round.

A maze where players can get lost in, wandering around in circles.
Feeling unnerved by the various unexplained noises lingering around
the environment. Deep fog, and several guiding lights admist the
invisible walls within the maze. Odd happenings, changes that 
you cant put together at first and odd, unnerving sounds as
you wander around to the final room. 

Collecting capsules, and clicking odd shaped lamps revealing not so helpful 
clues, the player is given somewhat of a purpose as they explore. 

----------------------------------------------------------------------------------------
Gameplay:
- During testing, the already existing creepy background music set the mood already. 
  This, included with the footsteps audio that can be heard, especially at the first 
  corners, creates an atmosphere thats similar to running away and fumbling with 
  obstacles (the hidden walls in this case), like in horror films.

----------------------------------------------------------------------------------------
Design Decisions:
- The first two doorways are purposefully harder to squeeze through. 
(From spawn point)
	- This is because this is the first player interaction with the audio.
	  and also a small choke/panic point where people hear footsteps and realise
	  that its not their own even when they stop. 

- Grid like map layout
	- This is to create a seamless loop where players can only follow one direction,
	  and even at the end, players reach the cross roads, depending on where they turn
	  feels like theres no progress. 
	- The trick is to always go forward, never doubling back.
	- I got lost in it while testing many times 

- Capsule collection
	- A motive for players to move around the map and explore, collecting all 26
	  capsules.

- Light flickering
	- Happy accident, with the clipping of my meshes and the options chosen for the 
	  light sources, they turn on and off automatically depending on the the player's 
	  actions, where they are placed in the map, where they are looking.
		- During testing, the audio played into this, when it suddenly cut off,
	 	  I turned around and the light went off also. Or the light would flicker
- Mesh glitching
	- Purposeful bad placement of objects to make their bodies bounce around or wiggle 
	  furiously. Adds to the haunted or spooky vibe.

- Hidden Walls
	- Hidden walls placed around the map to force people to listen to the audio
	  placed as a deterrent and to slow players down.

- Lamp Clicking
	- To give a seemingly useless clue. Forward, and also to help people realise
	  that you can click things to make events happen. Eg. Capsules collection

- Audio 
	- BGM
	  I knew I wanted a creepy background music that would set the mood along with
	  the dark area.

	- Children Laughing
	  Something typical of horror games and films, the sound of an innocent being
	  in an obviously creepy area makes people nervous.

	- Door Slam/Clap
	  Jumpscare, to startle people momentarily, and also used to cover the gaps
	  and radius of the area between the BGM and the rumbling sound effect.
	
	- Light Flicker Sound Effect
	  A sound effect that matches the environment. Also, a sound effect that has
	  a sudden and rather abrupt ending that startles players.
	  
	- Footsteps Sound EFfect
	  To give players the impression that something is following them around at 
	  certain areas. Even more unnerving when the player stops but the footsteps
	  continue. 
		- Was originally going to be used for the player's footsteps, 
		  however the player's walking bob animation didn't match the audio 
		  of the footsteps and was removed. 
		- In addition, with scripting, when players held down the W key, the 
		  keycode recognises that it has been pressed, but only once. 
		  So the footsteps sounds fades away. And, the footsteps cannot be 
		  looped either, because then the walking sound effect becomes obvious 
		  when players stop moving and becomes rather sloppy.

		- As a result, it was transferred to Radius Audio, like the majority 
		  of the environment. 

	- Rumble Sound Effect
	 	- Rumbling sound effect used in the throne room, matches the rumbling
		  columns. Creates unnerving feeling.
	  

- Brick wall layout
	- There are two different types of illusion, the one used into the throne room, 
	  and the main one, and the other which is the flip of that. (Two front- 1 mid
	  back as main, 1mid front, 2 back on either side)
	- In addition, theres the normal perception illusion, where when things are far 
	  away, it looks like its a blocked alleyway. (First two entrances on either 
	  side of spawn point.)
	- In the area, where all of the alleyway entrances coincide are a all the same
	  type of illusion wall seen. This is so that from the outside, players can't
	  tell the difference and continues to sell the 'maze'.
	- I used a polygon/circular type area for the cross roads, so that there was a
	  gap where players could 'choose' where to go.

- Asset placement in alleyways
	- They mirror each other, in parallel format. So that when you turn the corner
	  it can give the effect that you never travelled that far, or simply to the 
	  other end of the alleyway. Even though you stand at the corner and see both
	  ends. Meant to confuse.

----------------------------------------------------------------------------------------
Failures and Abandoned Design Decisions:
- Light Fog and Particle System Fog Effect
	- Didn't work because the fog would highlight the door frame out for everyone,
	  and ruin the effect of it being a dead-end or a hidden passageway.

- Different scenes, game start screen, game end screen, introduction screen
	- I decided not to do this, because I wanted to throw players straight into the 
	  environment, instead of giving them time to prepare.

- Light Design Decisions
	- Following the fog, I would have used little lights emitted from the small 
	  inside lamps that would help guide them, like a lighthouse, to help navigate.
	  However, this, like the fog, with their placements easily illuminated the walls.
	  Ruining the maze like effect. - This also couldn't be blocked by hidden walls.

- Poltergist happenings
	- The mesh triggers, or collisions didn't work. 
	  Originally, I wanted different things such as wall changes, or spawning 
	  points around the environment to occur when players reach a certain area 
	  of the map. So that it would give the impression that the place is haunted.
		- Eg. Walk too close somewhere and the poster from the wall will fall 
		      Or, 
	  
	- Also, I was thinking of possible hidden wall switches, that if the player 
	  walked to a certain spot, or switched on a certain lamp, different blocking
	  walls would appear, allowing or denying them access to certain areas.
	  (However, this meant that the map would need to be much larger for this to work
	  and was dropped because of scoping, in addition to the mesh triggers being an
	  unavaliable tool.)

	- As such, abandoning it finally, I decided to go more into audio.

- Clicking Lamp Teleport
	- I thought it would be fun to have random teleportation around the map.
	  Where when players clicked on the map, it would trigger and send them 
	  somewhere else around the map. (Spawn elsewhere facing the lamp)
	- This would be implemented with more time, and also, I would need to figure out
	  the randomisation, of spawning areas.
	- Also, possibly you might need a bigger map for this effect to work?

----------------------------------------------------------------------------------------
Idea for some other time:
- A deceptively blank area with a door that says exit on the other side
  but really has a bunch of hidden walls and doors inside. And the only
  method of navigation is following a series of lines on the roof.
  That changes depending on where people walk or interact. Player starts
  on the opposite end of the exit. Also the spawn point so players can 
  reset whenever they get stuck.

  Players then can map the area.

----------------------------------------------------------------------------------------
Games drawn from:
- Slender Woods (PC)
	- Atmosphere, the feeling of being chased.
	- Idea of purpose, collecting capsules in this case, instead of Notes.
	- Drawing people to a certain area on purpose.

- The Blind Knight (Android choose your adventure)
	- The effects of audio on a player.

----------------------------------------------------------------------------------------
Tools Used:
- https://www.youtube.com/channel/UCB5T86uTAdghJhEfJdvu5iQ 
Horror BGM, Free, by Halloween Horror Music

- https://www.youtube.com/watch?v=MdyLzv4Q6Kw
Neon Light Flicker Sound Effect, Free, by Vinnie Phakeovilay

- https://www.youtube.com/watch?v=LrchScd806w
Introduction to Unity 3D sound, by Learn Everything Fast

- https://www.youtube.com/watch?v=B6Ajq-3ivvY
Footstep Free Sound Effect, Free, by Free SoundEffects

- https://www.youtube.com/watch?v=NaJ1CsvRCc0
SoundEffects Heaven, Jump/Royalty Free Sound Effect, Free, by snottyboy

- https://www.youtube.com/watch?v=BgqsbwYLjds
Slam Sound Effect, Free Sound Effects, by SoundFREX

- Youtube Sound library, Children laughing SFX, Myles Moss

https://docs.unity3d.com/Manual/class-AudioSource.html and Bowl and Cereal Youtube,
Unity Tutorial: Playing Audio (Music and Sound Effects)
- Used in jump and introduction to sound effects and audio scripting

- Rumbling Sound Effect, Youtube Sound library, Rumbling Sound Effect, 
GamingSoundEffects

- Code from my old projects, bumper blocks and riddle room.

- All assets are of my own making in Maya, imported into Unity.
